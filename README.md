# Clamav Anti-virus

[![pipeline status](https://gitlab.com/radicalspam-k8s/clamav/badges/master/pipeline.svg)](https://gitlab.com/radicalspam-k8s/clamav/-/commits/master)
[![coverage report](https://gitlab.com/radicalspam-k8s/clamav/badges/master/coverage.svg)](https://gitlab.com/radicalspam-k8s/clamav/-/commits/master)

- [Sources](https://github.com/Cisco-Talos/clamav-devel)
- [Manuel utilisateur](https://www.clamav.net/documents/clam-antivirus-user-manual)
- [Tests externes](https://wiki.ubuntu.com/MOTU/Clamav/TestingProcedures)

## Versions

- Alpine: 3.13.2
- Clamav: [0.103.1](https://github.com/Cisco-Talos/clamav-devel/tree/dev/0.103.1)

## Utilisation avec Docker

```bash
docker run -d --name clamav -p 3310:3310 registry.gitlab.com/radicalspam-k8s/clamav:0.103.1
docker run -it --rm --link clamav:clamav -v $(pwd):/scan -w /scan registry.gitlab.com/radicalspam-k8s/clamav:0.103.1 sh
clamdtop clamav:3310
clamscan --help
```

## Utilisation avec Kubernetes

> S'installe dans le namespace en cours

```bash
kubectl apply -f k8s-manifests/*
```

## TODO

- [ ] Script wait pour attendre la disponibilité du daemon clamd au démarrage
- [ ] ChangeLog
- [ ] Optimisations
- [ ] Tag du projet git avec la même version que clamav - Non, mauvaise pratique
- [ ] Ajout de signatures [unofficial-sigs](https://github.com/extremeshok/clamav-unofficial-sigs)
- [x] Ajout d'un [exporter Prometheus](https://github.com/r3kzi/clamav-prometheus-exporter)
- [ ] Readness exporter et autres config
- [ ] kube: Voir pourquoi le cronjob créé un pod à chaque exécution sans le supprimer ?
- [ ] kube: Revoir liveness/readness
- [ ] kube: Lint et Testing
- [ ] kube: Helm chart ?

