ARG ALPINE_VERSION=3.13.2

FROM alpine:${ALPINE_VERSION}

ARG CLAMAV_VERSION=0.103.1

LABEL maintainer="stephane.rault@radicalspam.org"

RUN set -eux \
    && apk add --no-cache --update \
    clamav-daemon=${CLAMAV_VERSION}-r0 \
    clamav=${CLAMAV_VERSION}-r0 \
    clamav-libunrar=${CLAMAV_VERSION}-r0 \
    ca-certificates \
    netcat-openbsd \
    procps \
    bash \
    ncurses \
    rsync \
    bind-tools \
    gnupg \
    curl \
    socat

COPY --chown=root:root clamd.conf /etc/clamav/

COPY --chown=root:root freshclam.conf /etc/clamav/

RUN clamconf -n

RUN freshclam --verbose --stdout \
    && install -d -m 755 -o clamav -g clamav /var/run/clamav \
    && install -d -m 755 -o clamav -g clamav /var/log/clamav-unofficial-sigs \ 
    && install -d -m 755 -o clamav -g clamav /var/lib/clamav-unofficial-sigs \ 
    && mkdir -p /etc/clamav-unofficial-sigs \
    && rm -rf /tmp/* /var/tmp/* /var/cache/apk/* /var/log/clamav

COPY clamav-unofficial-sigs-master.conf /etc/clamav-unofficial-sigs/master.conf
COPY os.conf /etc/clamav-unofficial-sigs/os.conf

COPY --chown=root:root clamav-unofficial-sigs.sh /usr/local/bin/

COPY --chown=root:root clamd-ping /usr/local/bin/

# RUN sed -i -e "s/bin\/ash/bin\/bash/" /etc/passwd
#RUN ["/bin/bash", "-c", "/usr/local/bin/clamav-unofficial-sigs.sh --force"]

USER clamav

WORKDIR /var/lib/clamav

EXPOSE 3310

CMD [ "clamd" ]

HEALTHCHECK CMD /usr/local/bin/clamd-ping
